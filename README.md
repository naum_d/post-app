# post-app

1. Для запуска приложения небходимо: `Node.js, NPM, MongoDB`
2. Запуск бэка: `cd back/ && npm run dev`
3. Запуск клиента: `cd front && npm start`

URL для MongoDB `mongodb://localhost:27017/post-app`, изменить можно в 
`./back/.env`

Должны быть доступны порты: `:8000 :3000`


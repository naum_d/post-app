import { combineReducers } from 'redux';

import { appStoreReducer } from './appStore/reducers';

export const reducers = combineReducers({
  appStore: appStoreReducer
});

import thunk from 'redux-thunk';
import { applyMiddleware, createStore } from 'redux';

import { reducers } from './reducers';

export const configStore = () => {
  const middleware = [thunk];

  return createStore(reducers, applyMiddleware(...middleware));
};

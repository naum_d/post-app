import API from '../../API';

export const APP_STORE_UPDATE_LIST = 'APP_STORE_UPDATE_LIST';
export const APP_STORE_UPDATE_STORE = 'APP_STORE_UPDATE_STORE';
export const APP_STORE_DELETE_STORE = 'APP_STORE_DELETE_STORE';

export const appStoreUpdateStore = ({ storeName, data }) => {
  return {
    type: APP_STORE_UPDATE_STORE,
    payload: { storeName, data }
  };
};

export const appStoreDeleteStore = (storeName) => {
  return {
    type: APP_STORE_DELETE_STORE,
    payload: { storeName }
  };
};

export const appStoreUpdateList = ({ storeName, method }) => {
  return {
    type: APP_STORE_UPDATE_LIST,
    payload: { storeName, method }
  };
};

export const appStoreLoadData = ({ storeName, dataSource }) => {
  return dispatch => new Promise((resolve, reject) => {
    dispatch(appStoreMakeRequest({ storeName, dataSource }))
      .then(resp => {
        dispatch(appStoreUpdateStore({ storeName, data: { data: resp, isLoading: false } }));
        resolve(resp);
      })
      .catch(error => {
        dispatch(appStoreUpdateStore({ storeName, data: { isLoading: false } }));
        reject(error);
      });
  });
};

export const appStoreLogin = ({ dataSource, data }) => {
  const storeName = 'user';
  return dispatch => new Promise(((resolve, reject) => {
    dispatch(appStoreMakeRequest({ storeName, dataSource }, 'POST', data))
      .then(resp => {
        dispatch(appStoreUpdateStore({ storeName, data: { data: resp, isLoading: false } }));
        localStorage.setItem('token', resp.token);
        resolve(resp);
      })
      .catch(error => {
        const { data } = error.response;
        dispatch(appStoreUpdateStore({ storeName, data: { data: {}, isLoading: false } }));

        reject(data);
      });
  }));
};

export const appStoreLogout = () => {
  return dispatch => {
    localStorage.clear();
    dispatch(appStoreDeleteStore('user'));
  };
};

export const appStoreMakeRequest = ({ storeName, dataSource }, method = 'GET', data = {}) => {
  return dispatch => new Promise(((resolve, reject) => {
    dispatch(appStoreUpdateStore({ storeName, data: { isLoading: true } }));

    API.request(dataSource, { method, data })
      .then(resp => resolve(resp))
      .catch(error => reject(error));
  }));
};

import * as ACTIONS from './actions';

const initStore = {
  data: null,
  isLoading: true,
  dataSource: null
};

export const appStoreReducer = (state = {}, action) => {
  const storeName = !!action.payload ? action.payload.storeName : null;
  const store = !!storeName ? state[storeName] : {};

  switch (action.type) {
    case ACTIONS.APP_STORE_UPDATE_LIST:
      return { ...state, [storeName]: { ...store, data: action.payload.method(store.data) } };

    case ACTIONS.APP_STORE_UPDATE_STORE:
      return { ...state, [storeName]: { ...initStore, ...store, ...action.payload.data } };

    case ACTIONS.APP_STORE_DELETE_STORE:
      delete state[storeName];
      return state;

    default:
      return state;
  }
};

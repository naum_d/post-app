import React from 'react';
import ReactDOM from 'react-dom';

import App from './containers/App';

import { configStore } from './store';
import { Provider } from 'react-redux';

const store = configStore();
const target = document.querySelector('#root');

const Root = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

ReactDOM.render(<Root />, target);

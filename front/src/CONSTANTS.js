export const ME = '/api/user/me';
export const POSTS = '/api/posts';
export const SIGN_IN = '/api/user';
export const LOGIN = '/api/user/login';
export const COMMENTS = '/api/comments';

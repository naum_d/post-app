const setupProxy = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    setupProxy('/api', {
      target: process.env.REACT_APP_API_SERVER,
      secure: true,
      changeOrigin: true,
    }),
  );
};

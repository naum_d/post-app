import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { Typography } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import { useDispatch, useSelector } from 'react-redux';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';

import * as CONST from '../../CONSTANTS';
import PostForm from './PostForm/PostForm';
import AppExpansion from '../Layouts/AppExpansion';
import { appStoreMakeRequest, appStoreUpdateList } from '../../store/appStore/actions';
import Comments from '../Comments/Comments';

const PostCard = props => {

  const { post, storeName } = props;

  const dispatch = useDispatch();
  const [userId, setUserId] = useState(0);
  const [showForm, setShowForm] = useState(false);
  const userStore = useSelector(state => state.appStore['user']);

  useEffect(() => {
    !!userStore && !!userStore.data && setUserId(userStore.data._id || 0);
  }, [userStore]);

  const handleDelete = id => {
    dispatch(appStoreMakeRequest({ dataSource: `${CONST.POSTS}/${id}` }, 'DELETE'))
      .then(() => {
        dispatch(appStoreUpdateList({ storeName, method: data => data.filter(item => item._id !== id) }));
      });
  };

  const renderBody = () => {
    const isOwner = post.user === userId;
    return (
      <Grid container alignItems="flex-start" alignContent="flex-start">
        <Grid item xs={isOwner ? 11 : 12}>
          {!showForm && (
            <Grid container direction="column">
              <Typography variant="body1" children={post.article} />
              <Comments postId={post._id} />
            </Grid>
          )}
          {showForm && <PostForm {...{ storeName }} method="PUT" formInit={post} />}
        </Grid>
        {isOwner && (
          <Grid item xs={1}>
            <IconButton onClick={() => setShowForm(!showForm)} children={<EditOutlinedIcon />} />
            <IconButton onClick={() => handleDelete(post._id)} children={<DeleteOutlineOutlinedIcon />} />
          </Grid>
        )}
      </Grid>
    );
  };

  return (
    <AppExpansion title={post.title} body={renderBody()} />
  );
};

PostCard.propTypes = {
  post: PropTypes.object,
  storeName: PropTypes.string
};

export default PostCard;

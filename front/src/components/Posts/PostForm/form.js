import * as Yup from 'yup';

export const TITLE = 'title';
export const ARTICLE = 'article';

export const INIT_FORM = { [TITLE]: '', [ARTICLE]: '' };

export const validation = Yup.object({
  [TITLE]: Yup.string().required('Required'),
  [ARTICLE]: Yup.string().required('Required')
});

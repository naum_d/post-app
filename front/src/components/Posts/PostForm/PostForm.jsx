import React, { Fragment } from 'react';
import * as PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import { useDispatch } from 'react-redux';

import * as FORM from './form';
import * as CONST from '../../../CONSTANTS';
import FormikInput from '../../FormikContainer/FormikInput';
import FormikContainer from '../../FormikContainer/FormikContainer';
import { appStoreMakeRequest, appStoreUpdateList } from '../../../store/appStore/actions';

const PostForm = props => {

  const { formInit, method, storeName } = props;

  const dispatch = useDispatch();

  const handleSubmit = (data, actions) => {
    const dataSource = `${CONST.POSTS}${method === 'PUT' ? `/${formInit._id}` : ''}`;

    dispatch(appStoreMakeRequest({ storeName, dataSource }, method, data))
      .then(resp => {
        const pushData = data => [resp, ...data];
        const replaceData = data => {
          const index = data.findIndex(item => item._id === resp._id);
          return [...data.slice(0, index), resp, ...data.slice(index + 1)];
        };

        actions.resetForm(FORM.INIT_FORM);
        actions.setSubmitting(false);
        dispatch(appStoreUpdateList({ storeName, method: method === 'POST' ? pushData : replaceData }));
      });
  };

  return (
    <Grid container>
      <Grid item xs={12}>
        <FormikContainer
          onSubmit={handleSubmit}
          initialValues={formInit}
          validationSchema={FORM.validation}
          renderFormFields={() => (
            <Fragment>
              <FormikInput label="Title" name={FORM.TITLE} />
              <FormikInput label="Article" name={FORM.ARTICLE} inputProps={{ multiline: true, rows: 3 }} />
            </Fragment>
          )}
        />
      </Grid>
    </Grid>
  );
};

PostForm.propTypes = {
  method: PropTypes.string,
  callback: PropTypes.func,
  formInit: PropTypes.object,
  storeName: PropTypes.string.isRequired
};

PostForm.defaultProps = {
  method: 'POST',
  formInit: FORM.INIT_FORM
};

export default PostForm;

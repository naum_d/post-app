import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { useDispatch, useSelector } from 'react-redux';
import { appStoreDeleteStore, appStoreLoadData } from '../../store/appStore/actions';

import PostCard from './PostCard';
import * as CONST from '../../CONSTANTS';
import PostForm from './PostForm/PostForm';
import AppExpansion from '../Layouts/AppExpansion';

const Posts = () => {

  const storeName = 'posts-store';
  const dataSource = CONST.POSTS;

  const dispatch = useDispatch();
  const [posts, setPosts] = useState([]);
  const postsStore = useSelector(state => state.appStore[storeName]);

  useEffect(() => {
    dispatch(appStoreLoadData({ storeName, dataSource }));
    return () => dispatch(appStoreDeleteStore(storeName));
  }, [dispatch, storeName, dataSource]);

  useEffect(() => {
    !!postsStore && setPosts(postsStore.data || []);
  }, [postsStore]);

  return (
    <Grid container justify="center">
      <Grid item xs={9}>
        <AppExpansion title="Add New Post" body={<PostForm {...{ storeName }} />} />

        {posts.map(post => (<PostCard key={post._id} {...{ post, storeName }} />))}
      </Grid>
    </Grid>
  );
};

Posts.propTypes = {};

export default Posts;

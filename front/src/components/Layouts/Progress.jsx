import React from 'react';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { fade } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  container: {
    position: 'absolute',
    top: '0',
    left: '0',
    bottom: '0',
    zIndex: 1200,
    backgroundColor: fade(theme.palette.background.default, 0.7)
  }
}));

const Progress = () => {

  const classes = useStyles();

  return (
    <Grid container component="div" direction="row" justify="center" alignItems="center" className={classes.container}>
      <Grid item component="div">
        <CircularProgress />
      </Grid>
    </Grid>
  );
};

export default Progress;

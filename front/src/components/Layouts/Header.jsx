import React from 'react';
import * as PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { useDispatch } from 'react-redux';
import { appStoreLogout } from '../../store/appStore/actions';

const useStyles = makeStyles(theme => ({
  title: {
    flexGrow: 1
  }
}));

const Header = props => {

  const { isLogin } = props;

  const classes = useStyles();
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(appStoreLogout());
  };

  return (
    <AppBar
      position="fixed">
      <Toolbar>
        <Typography variant="h6" className={classes.title} children={'Post App'} />

        {isLogin && <Button color="inherit" onClick={handleLogout} children="Logout" />}
      </Toolbar>
    </AppBar>
  );
};

Header.propTypes = {
  isLogin: PropTypes.bool.isRequired
};

export default Header;

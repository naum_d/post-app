import React, { Fragment } from 'react';
import * as PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import FormikContainer from '../FormikContainer/FormikContainer';
import FormikInput from '../FormikContainer/FormikInput';
import { useDispatch } from 'react-redux';
import { appStoreMakeRequest, appStoreUpdateList } from '../../store/appStore/actions';
import * as CONST from '../../CONSTANTS';
import * as Yup from 'yup';

const CommentForm = props => {

  const { postId, storeName } = props;

  const POST = 'post';
  const COMMENT = 'comment';
  const formInit = { [COMMENT]: '', [POST]: postId };
  const validation = Yup.object({ [COMMENT]: Yup.string().required('Required') });

  const dispatch = useDispatch();

  const handleSubmit = (data, actions) => {
    dispatch(appStoreMakeRequest({ storeName, dataSource: CONST.COMMENTS }, 'POST', data))
      .then(resp => {
        actions.resetForm(formInit);
        actions.setSubmitting(false);
        dispatch(appStoreUpdateList({ storeName, method: data => [resp, ...data] }));
      });
  };

  return (
    <Grid container>
      <Grid item xs={12}>
        <FormikContainer
          onSubmit={handleSubmit}
          initialValues={formInit}
          validationSchema={validation}
          renderFormFields={() => (
            <Fragment>
              <FormikInput label="Comment" name={COMMENT} />
            </Fragment>
          )}
        />
      </Grid>
    </Grid>
  );
};

CommentForm.propTypes = {
  postId: PropTypes.string.isRequired,
  storeName: PropTypes.string.isRequired
};

export default CommentForm;

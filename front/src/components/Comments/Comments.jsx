import React, { Fragment, useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import { useDispatch, useSelector } from 'react-redux';
import ListItemText from '@material-ui/core/ListItemText';

import CommentForm from './CommentForm';
import * as CONST from '../../CONSTANTS';
import { appStoreDeleteStore, appStoreLoadData } from '../../store/appStore/actions';

const Comments = props => {

  const { postId } = props;
  const storeName = `comments-store-${postId}`;
  const dataSource = `${CONST.COMMENTS}/${postId}`;

  const dispatch = useDispatch();
  const [comments, setComments] = useState([]);
  const commentsStore = useSelector(state => state.appStore[storeName]);

  useEffect(() => {
    dispatch(appStoreLoadData({ storeName, dataSource }));
    return () => dispatch(appStoreDeleteStore(storeName));
  }, [dispatch, storeName, dataSource]);

  useEffect(() => {
    !!commentsStore && setComments(commentsStore.data || []);
  }, [commentsStore]);

  return (
    <Grid container>
      <Grid item xs={12}>
        <CommentForm {...{ postId, storeName }} />
        <List>
          {comments.map(comment => (
            <Fragment key={comment._id}>
              <ListItem>
                <ListItemText
                  primary={comment.user.email}
                  secondary={comment.comment}
                />
              </ListItem>
              <Divider variant="fullWidth" component="li" />
            </Fragment>
          ))}
        </List>
      </Grid>
    </Grid>
  );
};

Comments.propTypes = {};

export default Comments;

import React from 'react';
import { Formik } from 'formik';
import * as PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';

import Progress from '../Layouts/Progress';
import { SubmitButton } from './SubmitButton';
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(theme => ({
  card: {
    position: 'relative',
    marginTop: theme.spacing(2),
    padding: theme.spacing(2, 3)
  }
}));

const FormikContainer = props => {

  const { actions, renderFormFields, onSubmit } = props;

  const classes = useStyles();

  const handleSubmit = (values, actions) => {
    onSubmit(JSON.stringify(values, null, 2), actions);
  };

  const renderForm = props => {
    const { handleSubmit, isValid, isSubmitting, initialValues } = props;

    return (
      <form onSubmit={handleSubmit}>
        <Grid container spacing={3} component="div">
          {isSubmitting && <Progress />}

          {renderFormFields(props)}

          <SubmitButton {...{ actions }} disabled={!isValid || isSubmitting} formData={initialValues} />
        </Grid>
      </form>
    );
  };

  return (
    <div className={classes.card}>
      <Formik
        {...props}
        render={renderForm}
        onSubmit={handleSubmit}
        enableReinitialize={true}
      />
    </div>
  );
};

FormikContainer.propTypes = {
  actions: PropTypes.func,
  formTitle: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
  validationSchema: PropTypes.any.isRequired,
  renderFormFields: PropTypes.func.isRequired
};

export default FormikContainer;

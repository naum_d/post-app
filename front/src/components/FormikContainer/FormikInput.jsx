import React from 'react';
import { Field, getIn } from 'formik';
import * as PropTypes from 'prop-types';

import AppInput from '../AppInput';

const FormikInput = props => {

  const { name, label, inputProps, onChange, ...other } = props;

  return (
    <Field
      name={name}
      render={({ form: { touched, errors, setFieldTouched, setFieldValue, handleBlur }, field: { value } }) => {
        const error = getIn(errors, name);
        const touch = getIn(touched, name);

        const handleChange = e => {
          const value = e.target.value;
          setFieldValue(name, value);
          setFieldTouched(name);
          onChange(e);
        };

        return (
          <AppInput
            {...{ ...other }}
            formControl={{ error: !!touch && !!error }}
            inputLabel={{ htmlFor: name, children: label }}
            formHelper={!!touch && !!error && { children: error }}
            inputProps={{ ...inputProps, value, name, onBlur: handleBlur, onChange: handleChange }}
          />
        );
      }}
    />
  );
};

FormikInput.propTypes = {
  label: PropTypes.string,
  onChange: PropTypes.func,
  inputProps: PropTypes.object,
  name: PropTypes.string.isRequired
};

FormikInput.defaultProps = {
  onChange: () => {}
};

export default FormikInput;

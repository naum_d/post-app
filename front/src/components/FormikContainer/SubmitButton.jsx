import React from 'react';
import * as PropTypes from 'prop-types';
import { Button, Grid } from '@material-ui/core';

export const SubmitButton = ({ disabled, actions, formData }) => {
  return (
    <Grid item xs={12}>
      <Grid container justify={!!actions ? 'space-between' : 'flex-end'} alignItems="center">
        {!!actions && actions(formData)}

        <Button color="primary" variant="contained" type="submit" disabled={disabled} children={'Submit'} />
      </Grid>
    </Grid>
  );
};

SubmitButton.propTypes = {
  actions: PropTypes.func,
  formData: PropTypes.any,
  disabled: PropTypes.bool.isRequired
};

import * as Yup from 'yup';

export const EMAIL = 'email';
export const PASSWORD = 'password';
export const PASSWORD_REPEAT = 'passwordRepeat';

export const INIT_LOGIN_FORM = { [EMAIL]: '', [PASSWORD]: '' };
export const INIT_SIGN_IN_FORM = { [EMAIL]: '', [PASSWORD]: '', [PASSWORD_REPEAT]: '' };

export const loginValidation = Yup.object({
  [EMAIL]: Yup.string().required('Required').email('Invalid email'),
  [PASSWORD]: Yup.string().required('Required')
});

export const signInValidation = Yup.object({
  [PASSWORD]: Yup.string().required('Required'),
  [PASSWORD_REPEAT]: Yup.string().oneOf([Yup.ref(PASSWORD), null], 'Passwords must match').required('Required'),
  [EMAIL]: Yup.string().required('Required').email('Invalid email')
});

import React, { Fragment, useState } from 'react';
import { useDispatch } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import * as FORM from './form';
import * as CONST from '../../CONSTANTS';
import AppTabs from '../Layouts/AppTabs';
import FormikInput from '../FormikContainer/FormikInput';
import { appStoreLogin } from '../../store/appStore/actions';
import FormikContainer from '../FormikContainer/FormikContainer';

const Auth = () => {

  const dispatch = useDispatch();
  const [tab, setTab] = useState(0);

  const handleLogin = (data, actions) => {
    dispatch(appStoreLogin({ dataSource: CONST.LOGIN, data }))
      .catch(error => {
        actions.setSubmitting(false);
        !!error && actions.setErrors(error.errors);
      });
  };

  const handleSignIn = (data, actions) => {
    dispatch(appStoreLogin({ dataSource: CONST.SIGN_IN, data }))
      .catch(error => {
        actions.setSubmitting(false);
        !!error && actions.setErrors(error.errors);
      });
  };

  return (
    <Grid container justify="center">
      <Grid item xs={8}>
        <Paper>
          <AppTabs  {...{ tab }} tabsList={['Login', 'Sign In']} handleChange={setTab} />

          {tab === 0 && (
            <FormikContainer
              onSubmit={handleLogin}
              initialValues={FORM.INIT_LOGIN_FORM}
              validationSchema={FORM.loginValidation}
              renderFormFields={() => (
                <Fragment>
                  <FormikInput label="Email" name={FORM.EMAIL} inputProps={{ autoFocus: true }} />
                  <FormikInput label="Password" name={FORM.PASSWORD} inputProps={{ type: 'password' }} />
                </Fragment>
              )}
            />
          )}

          {tab === 1 && (
            <FormikContainer
              onSubmit={handleSignIn}
              initialValues={FORM.INIT_SIGN_IN_FORM}
              validationSchema={FORM.signInValidation}
              renderFormFields={() => (
                <Fragment>
                  <FormikInput label="Email" name={FORM.EMAIL} inputProps={{ autoFocus: true }} />

                  <Grid item xs={12}>
                    <Grid container spacing={2}>
                      <Grid item xs={6}>
                        <FormikInput label="Password" name={FORM.PASSWORD} inputProps={{ type: 'password' }} />
                      </Grid>
                      <Grid item xs={6}>
                        <FormikInput
                          label="Repeat Password"
                          name={FORM.PASSWORD_REPEAT}
                          inputProps={{ type: 'password' }}
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </Fragment>
              )}
            />
          )}
        </Paper>
      </Grid>
    </Grid>
  );
};

export default Auth;

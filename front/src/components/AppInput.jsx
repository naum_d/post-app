import React from 'react';
import * as PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import makeStyles from '@material-ui/core/styles/makeStyles';
import FormHelperText from '@material-ui/core/FormHelperText';

const useStyles = makeStyles(theme => ({
  formControl: {
    width: '100%'
  },
  grid: {
    marginTop: theme.spacing(1)
  }
}));

const AppInput = props => {

  const { formControl, inputLabel, inputProps, formHelper } = props;

  const classes = useStyles();

  return (
    <Grid item xs={12}>
      <FormControl className={classes.formControl} {...formControl}>
        {!!inputLabel && !!inputLabel.children && <InputLabel {...inputLabel} />}

        <Input {...{ ...inputProps }} />

        {!!formHelper && <FormHelperText {...formHelper} />}
      </FormControl>
    </Grid>
  );
};

AppInput.propTypes = {
  value: PropTypes.string,
  inputProps: PropTypes.object,
  inputLabel: PropTypes.object,
  formControl: PropTypes.object,
};

export default AppInput;

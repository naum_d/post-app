import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import makeStyles from '@material-ui/styles/makeStyles';
import CssBaseline from '@material-ui/core/CssBaseline';

import * as CONST from '../CONSTANTS';
import Auth from '../components/Auth/Auth';
import Main from '../components/Layouts/Main';
import Posts from '../components/Posts/Posts';
import Header from '../components/Layouts/Header';
import { appStoreLoadData } from '../store/appStore/actions';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex'
  }
}));

const App = () => {

  const classes = useStyles();

  const dispatch = useDispatch();
  const [isLogin, setIsLogin] = useState(false);
  const userStore = useSelector(state => state.appStore['user']);

  useEffect(() => {
    setIsLogin(!!localStorage.getItem('token'));
  }, [userStore]);

  useEffect(() => {
    isLogin && !userStore && dispatch(appStoreLoadData({ storeName: 'user', dataSource: CONST.ME }));
  }, [isLogin, userStore, dispatch]);

  return (
    <div className={classes.root}>
      <CssBaseline />

      <Header {...{ isLogin }} />
      <Main>
        {!isLogin && <Auth />}
        {isLogin && <Posts />}
      </Main>
    </div>
  );
};

App.propTypes = {};

export default App;

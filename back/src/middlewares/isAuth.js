import jwt from 'jsonwebtoken';

export const isAuth = (req, res, next) => {
  const token = req.headers['x-access-token'] || req.headers['authorization'];
  if (!token) return res.status(401).send('Unauthorized');

  try {
    req.user = jwt.verify(token, process.env.JWT_SIGNATURE);
    next();
  } catch (e) {
    return res.status(400).send('Invalid token.');
  }
};

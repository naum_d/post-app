import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import Joi from 'joi';

const UserSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true, maxlength: 50 },
  password: { type: String, required: true, maxlength: 100 }
});

UserSchema.statics.validateForm = form => {
  const schema = Joi.object({
    email: Joi.string().max(50).required().email(),
    password: Joi.string().max(100).required()
  }).options({ stripUnknown: true });

  return schema.validate(form);
};

UserSchema.methods.generateAuthToken = function() {
  return jwt.sign({ _id: this._id, email: this.email }, process.env.JWT_SIGNATURE);
};

UserSchema.pre('remove', function(next) {
  this.model('Post').deleteMany({ user: this._id }, next);
  this.model('Comment').deleteMany({ user: this._id }, next);
});

const User = mongoose.model('User', UserSchema);

export default User;

import mongoose from 'mongoose';

import User from './user.model';
import Post from './post.model';
import Comment from './comment.model';

export const connectDb = () => {
  return mongoose.connect(process.env.DATABASE_URL);
};

export default { User, Post, Comment };

import mongoose from 'mongoose';
import Joi from 'joi';

const PostSchema = new mongoose.Schema({
  title: { type: String, required: true, max: 100 },
  article: { type: String, required: true, max: 1000 },
  user: { type: mongoose.Schema.Types.ObjectID, ref: 'User' }
});

PostSchema.statics.validateForm = form => {
  const schema = Joi.object({
    title: Joi.string().max(100).required(),
    article: Joi.string().max(1000).required()
  }).options({ stripUnknown: true });

  return schema.validate(form);
};

PostSchema.pre('remove', function(next) {
  this.model('Comment').deleteMany({ user: this._id }, next);
});

const Post = mongoose.model('Post', PostSchema);

export default Post;

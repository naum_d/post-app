import mongoose from 'mongoose';
import Joi from 'joi';

const CommentSchema = new mongoose.Schema({
  comment: { type: String, required: true, max: 1000 },
  user: { type: mongoose.Schema.Types.ObjectID, ref: 'User', required: true },
  post: { type: mongoose.Schema.Types.ObjectID, ref: 'Post', required: true }
});

CommentSchema.statics.validateForm = form => {
  const schema = Joi.object({
    comment: Joi.string().max(1000).required(),
    post: Joi.string().required()
  });

  return schema.validate(form);
};

const Comment = mongoose.model('Comment', CommentSchema);

export default Comment;

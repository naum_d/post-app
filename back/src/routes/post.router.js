import Post from '../models/post.model';

import { isAuth } from '../middlewares/isAuth';
import { getErrors } from '../../services/getErrors';

export const PostRouter = app => {
  app.get('/posts', async (req, res) => {
    const posts = await Post.find();
    return res.send(posts);
  });

  app.get('/posts/:id', async (req, res) => {
    try {
      const post = await Post.findById(req.params.id);
      return res.send(post);
    } catch (e) {
      return res.status(500).send(e);
    }
  });

  app.post('/posts', isAuth, async (req, res) => {
    try {
      const { error } = Post.validateForm(req.body);
      if (error) return res.status(400).send(getErrors(error));

      const post = await Post.create({
        ...req.body,
        user: req.user._id
      });

      return res.send(post);
    } catch (e) {
      return res.status(500).send(e);
    }
  });

  app.put('/posts/:id', isAuth, async (req, res) => {
    try {
      const { error } = Post.validateForm(req.body);
      if (error) return res.status(400).send(getErrors(error));

      await Post.updateOne({ _id: req.params.id }, { ...req.body });
      const post = await Post.findById(req.params.id);

      return res.send(post);
    } catch (e) {
      return res.status(500).send(e);
    }
  });

  app.delete('/posts/:id', isAuth, async (req, res) => {
    try {
      const post = await Post.findById(req.params.id);
      const result = !!post ? await post.remove() : null;

      return res.send(result);
    } catch (e) {
      return res.status(500).send(e);
    }
  });
};

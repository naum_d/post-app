import Comment from '../models/comment.model';
import { isAuth } from '../middlewares/isAuth';
import { getErrors } from '../../services/getErrors';

export const CommentRouter = app => {
  app.get('/comments/:postId', async (req, res) => {
    try {
      const comments = await Comment.find({ post: req.params.postId }).populate('user', 'email');
      return res.send(comments);
    } catch (e) {
      return res.status(500).send(e);
    }
  });

  app.post('/comments', isAuth, async (req, res) => {
    try {
      const { error } = Comment.validateForm(req.body);
      if (error) return res.status(400).send(getErrors(error));

      let comment = await Comment.create({ ...req.body, user: req.user._id });
      comment = await comment.populate('user', 'email').execPopulate();
      return res.send(comment);
    } catch (e) {
      return res.status(500).send(e);
    }
  });
};

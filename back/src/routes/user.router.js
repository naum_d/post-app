import bcrypt from 'bcrypt';

import User from '../models/user.model';
import { isAuth } from '../middlewares/isAuth';
import { genError, getErrors } from '../../services/getErrors';

const UserRouter = app => {
  app.post('/user', async (req, res) => {
    try {
      const { email, password } = req.body;

      const { error } = User.validateForm(req.body);
      if (!!error) return res.status(400).send(getErrors(error));

      await User.findOne({ email }, (err, docs) => {
        if (!!docs) return res.status(400).send(genError('email', 'Invalid email'));
      });

      const hashPassword = await bcrypt.hash(password, 10);
      const user = new User({ email, password: hashPassword });
      const token = await user.generateAuthToken();
      await user.save();

      return res.send({ _id: user._id, email: user.email, token });
    } catch (e) {
      return res.status(500).send(e);
    }
  });

  app.post('/user/login', async (req, res) => {
    try {
      const { error } = User.validateForm(req.body);
      if (!!error) return res.status(400).send(getErrors(error));

      const { email, password } = req.body;
      const user = await User.findOne({ email });

      if (!user) {
        return res.status(400).send(genError('email', 'Invalid email'));
      }
      else {
        const comparePassword = await bcrypt.compare(password, user.password);
        if (!comparePassword) return res.status(400).send(genError('password', 'Invalid password'));
      }

      const token = user.generateAuthToken();
      return res.send({ _id: user._id, email: user.email, token });
    } catch (e) {
      return res.status(500).send(e);
    }
  });

  app.get('/user/me', isAuth, async (req, res) => {
    const me = await User.findById(req.user._id).select('-password');
    return res.send(me);
  });

  app.delete('/user/:id', isAuth, async (req, res) => {
    const user = await User.findById(req.params.id);

    const result = !!user ? await user.remove().select('-password') : null;

    return res.send(result);
  });
};

export default UserRouter;

import express from 'express';

import UserRouter from './user.router';
import { PostRouter } from './post.router';
import { CommentRouter } from './comment.router';

const app = express();

UserRouter(app);
PostRouter(app);
CommentRouter(app);

export default app;

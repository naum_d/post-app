import 'dotenv/config';
import cors from 'cors';
import express from 'express';

import routes from './routes';
import { connectDb } from './models';

const app = express();

const port = process.env.PORT || 8000;

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api', routes);

app.get('/', (req, res) => {
  return res.status(200).send('Welcome to post-app API!');
});

connectDb().then(() => {
  app.listen(port, () => {
    console.log(`Available on http://localhost:${port}`);
  });
});

export const getErrors = error => {
  const err = error.details[0];
  return { errors: { [err.path[0]]: err.message } };
};

export const genError = (field, message) => {
  return { errors: { [field]: message } };
};
